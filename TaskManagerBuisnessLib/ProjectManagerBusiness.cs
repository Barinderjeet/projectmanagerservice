﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectManagerDataLib;
using ProjectManagerEntitiesLib;

namespace ProjectManagerBuisnessLib
{
    public class ProjectManagerBusiness
    {
        public void Add(Tasks item)
        {
            using (DataContext dbcontext = new DataContext())
            {
                dbcontext.Tasks.Add(item);
                dbcontext.SaveChanges();
            }
        }

        public List<Tasks> Delete(int id)
        {
            using (var ctx = new DataContext())
            {
                var task = ctx.Tasks.Where(s => s.TaskID == id)
                    .FirstOrDefault();
                ctx.Entry(task).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();

                return ctx.Tasks.ToList();
            }
        }
        public void test()
        {

        }
        public List<Tasks> UpdateEndDate(Tasks item)
        {
            using (DataContext dbcontext = new DataContext())
            {
                var context = dbcontext.Tasks.SingleOrDefault(x => x.TaskID == item.TaskID);
                context.EndDate = System.DateTime.Now;
                context.Flag = 1;
                context.Status = "1";
                dbcontext.SaveChanges();
                return dbcontext.Tasks.ToList();
            }
        }

        public void Update(Tasks item)
        {
            using(DataContext dbContext = new DataContext())
                {
                var context = dbContext.Tasks.SingleOrDefault(x => x.TaskID == item.TaskID);
                context.TaskID = item.TaskID;
                context.Task = item.Task;
                context.ParentID = item.ParentID;
                context.Priority = item.Priority;
                context.StartDate = item.StartDate;
                context.EndDate = item.EndDate;
                context.TaskOwner = item.TaskOwner;
                dbContext.SaveChanges();                   
                }
        }

        public List<Tasks> GetAll()
        {
            using (DataContext dbcontext = new DataContext())
            {
                return dbcontext.Tasks.ToList();
            }
        }

        public Tasks GetByTaskName(string TaskName)
        {
            using (DataContext dbcontext = new DataContext())
            {
                var context = dbcontext.Tasks.SingleOrDefault(x => x.Task == TaskName);
                return context;
            }
        }

        public Tasks GetById(int TaskID)
        {
            using (DataContext dbcontext = new DataContext())
            {
                var context = dbcontext.Tasks.SingleOrDefault(x => x.TaskID == TaskID);
                return context;
            }
        }
        public void AddParenTask(ParentTasks item)
        {
            using (DataContext dbcontext = new DataContext())
            {
                dbcontext.ParentTasks.Add(item);
                dbcontext.SaveChanges();
            }
        }

        public void AddProject(Projects item)
        {
            using (DataContext dbcontext = new DataContext())
            {
                dbcontext.Projects.Add(item);
                dbcontext.SaveChanges();
            }
        }

        public List<Projects> DeleteProjects(int id)
        {
            using (var ctx = new DataContext())
            {
                var project = ctx.Projects.Where(s => s.ProjectID == id)
                    .FirstOrDefault();
                ctx.Entry(project).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();

                return ctx.Projects.ToList();
            }
        }

        public void UpdateProjects(Projects item)
        {
            using (DataContext dbContext = new DataContext())
            {
                var context = dbContext.Projects.SingleOrDefault(x => x.ProjectID == item.ProjectID);
                context.ProjectID = item.ProjectID;
                context.Project = item.Project;
                context.Priority = item.Priority;
                context.StartDate = item.StartDate;
                context.EndDate = item.EndDate;
                dbContext.SaveChanges();
            }
        }
        public List<Projects> GetAllProjects()
        {
            using (DataContext dbcontext = new DataContext())
            {
                return dbcontext.Projects.ToList();
            }
        }

        public List<Users> GetAllUsers()
        {
            using (DataContext dbcontext = new DataContext())
            {
                return dbcontext.Users.ToList();
            }
        }
        public void AddUser(Users item)
        {
            using (DataContext dbcontext = new DataContext())
            {
                dbcontext.Users.Add(item);
                dbcontext.SaveChanges();
            }
        }

        public List<Users> DeleteUser(int id)
        {
            using (var ctx = new DataContext())
            {
                var user = ctx.Users.Where(s => s.UserID == id)
                    .FirstOrDefault();
                ctx.Entry(user).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();

                return ctx.Users.ToList();
            }
        }

        public void UpdateUser(Users item)
        {
            using (DataContext dbContext = new DataContext())
            {
                var context = dbContext.Users.SingleOrDefault(x => x.UserID == item.UserID);
                context.UserID = item.UserID;
                context.FirstName = item.FirstName;
                context.LastName = item.LastName;
                context.EmployeeID = item.EmployeeID;
                
                dbContext.SaveChanges();
            }
        }

        public List<ParentTasks> GetAllParenTask()
        {
            using (DataContext dbcontext = new DataContext())
            {
                return dbcontext.ParentTasks.ToList();
            }
        }


    }
}
