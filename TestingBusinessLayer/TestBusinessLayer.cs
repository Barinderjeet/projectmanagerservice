﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ProjectManagerEntitiesLib;
using ProjectManagerBuisnessLib;


namespace TestingBusinessLayer
{
    [TestFixture]
    public class TestBusinessLayer
    {

        [Test]
        public void TestGetAll()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            int count = obj.GetAll().Count;
            Assert.Greater(count, 0);
        }

        [Test]
        public void TestGetTaskId()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Tasks itemId = obj.GetById(9);
            Assert.AreEqual(9, itemId.TaskID);
        }

        [Test]
        public void TestTaskAdd()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Tasks item = new Tasks();
            item.Task = "TaskName1";
            item.ParentID = 15;
            item.Priority = 9;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.Add(item);
            Tasks testItem = obj.GetByTaskName("TaskName1");
            Assert.AreEqual("TaskName1", testItem.Task);
        }

        [Test]
        public void TestDelete()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.Delete(4);
            Tasks Task = obj.GetById(4);
            Assert.AreEqual(null, Task);
        }

        [Test]
        public void TestTaskUpdate()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Tasks item = new Tasks();
            item.TaskID = 12;
            item.Task = "TaskName1";
            item.Priority = 15;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.Update(item);
            Tasks updatedItems = obj.GetByTaskName("TaskName1");
            Assert.AreEqual(15, updatedItems.Priority);
                
        }

        [Test]
        public void TestEndaTaskUpdate()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Tasks item = new Tasks();
            item.TaskID = 12;
            item.Task = "TaskName1";
            item.Priority = 9;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.UpdateEndDate(item);
            Tasks itemAfterUpdate = obj.GetByTaskName("TaskName1");
            Assert.AreEqual(1, itemAfterUpdate.Flag);
        }
    }
}
