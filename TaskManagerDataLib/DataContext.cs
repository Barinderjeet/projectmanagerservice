﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

using ProjectManagerEntitiesLib;

namespace ProjectManagerDataLib
{
    public class DataContext:DbContext
    {
        public DataContext() : base("name= dbConnString")
        {
        }
        public DbSet<Tasks> Tasks { get; set; }
        public DbSet<Projects> Projects { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<ParentTasks> ParentTasks { get; set; }
    }
}
