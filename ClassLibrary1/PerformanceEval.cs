﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBench;
using NBench.Util;
using ProjectManagerBuisnessLib;
using ProjectManagerEntitiesLib;

namespace PerformanceEval

{
    public class PerformanceEval
    {
        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be raapidly executed ",
            NumberOfIterations = 1, RunMode = RunMode.Throughput,
            RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchMarkAddUpdate()
        {
            Tasks task = new Tasks();
            task.Task = "Test1";
            task.ParentID = 2018;
            task.StartDate = System.DateTime.Now;
            task.EndDate = System.DateTime.Now; ;
            task.Flag = 0;
            task.Priority = 10;
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.Add(task);
            obj.Update(task);
            obj.UpdateEndDate(task);

        }

        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be raapidly executed ",
            NumberOfIterations = 1, RunMode = RunMode.Throughput,
            RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchMarkAddProject()
        {
            Projects task = new Projects();
            task.Project = "Test1";           
            task.StartDate = System.DateTime.Now;
            task.EndDate = System.DateTime.Now; ;
            task.Flag = 0;
            task.Priority = 10;
         
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.AddProject(task);
            obj.UpdateProjects(task);
            

        }

        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be raapidly executed ",
           NumberOfIterations = 1, RunMode = RunMode.Throughput,
           RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchMarkAddUser()
        {
            Users task = new Users();
            task.FirstName = "Bari";
            task.LastName = "Singh";
            task.EmployeeID = 10;

            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.AddUser(task);
            obj.UpdateUser(task);

        }


        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be raapidly executed ",
           NumberOfIterations = 1, RunMode = RunMode.Throughput,
           RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]

        public void BenchMarkView()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.GetAll();
        }

        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be raapidly executed ",
         NumberOfIterations = 1, RunMode = RunMode.Throughput,
         RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]

        public void BenchMarkGetByTaskID()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.GetById(2012);
        }

        [PerfCleanup]
        public void CleanUp()
        {

        }
    }
}
