﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProjectManagerBuisnessLib;
using ProjectManagerEntitiesLib;

namespace ProjectManagerAPI.Controllers
{
    public class ProjectManagerController : ApiController
    {
        [Route("PostTask")]
        [HttpPost]
        public IHttpActionResult PostTask(Tasks task)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK);
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.Add(task);
            return Ok("Record Added");

        }

        [Route("DeleteTask")]
        [HttpDelete]
        public List<Tasks> DeleteTask(int id)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.Delete(id);
        }

        [Route("UpdateTask")]
        [HttpPut]
        public IHttpActionResult PutTask(Tasks task)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.Update(task);
            return Ok("Record Updated");
        }

        [Route("EndTask")]
        [HttpPut]
        public List<Tasks> UpdateEngDate(Tasks item)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.UpdateEndDate(item);
        }

        [Route("GetAll")]
        [HttpGet]
        public List<Tasks> GetAll()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.GetAll();
        }

        [Route("GetByTaskName")]
        [HttpGet]
        public Tasks GetByTaskName(string TaskName)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.GetByTaskName(TaskName);
        }

        [Route("GetById")]
        [HttpGet]
        public Tasks GetById(int TaskId)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.GetById(TaskId);
        }




        [Route("PostProject")]
        [HttpPost]
        public IHttpActionResult PostProject(Projects task)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK);
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.AddProject(task);
            return Ok("Record Added");

        }

        [Route("DeleteProject")]
        [HttpDelete]
        public List<Projects> DeleteProject(int id)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.DeleteProjects(id);
        }

        [Route("UpdateProject")]
        [HttpPut]
        public IHttpActionResult PutProject(Projects task)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.UpdateProjects(task);
            return Ok("Record Updated");
        }


        [Route("GetAllProject")]
        [HttpGet]
        public List<Projects> GetAllProjects()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.GetAllProjects();
        }

        [Route("GetAllUsers")]
        [HttpGet]
        public List<Users> GetAllUsers()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.GetAllUsers();
        }
        [Route("PostUser")]
        [HttpPost]
        public IHttpActionResult PostUser(Users task)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK);
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.AddUser(task);
            return Ok("Record Added");

        }

        [Route("DeleteUser")]
        [HttpDelete]
        public List<Users> DeleteUser(int id)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.DeleteUser(id);
        }

        [Route("UpdateUser")]
        [HttpPut]
        public IHttpActionResult PutUser(Users task)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.UpdateUser(task);
            return Ok("Record Updated");
        }

        [Route("PostParenTask")]
        [HttpPost]
        public IHttpActionResult PostParentTask(ParentTasks task)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK);
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.AddParenTask(task);
            return Ok("Record Added");

        }

        [Route("GetAllParenTask")]
        [HttpGet]
        public List<ParentTasks> GetAllParenTask()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.GetAllParenTask();
        }

    }
}
