﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;


namespace ProjectManagerAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            var Cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(Cors);
            config.Routes.MapHttpRoute(
                name: "TaskManager",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional },
                constraints: new {controller ="TaskManager" }

            );
        }
    }
}
