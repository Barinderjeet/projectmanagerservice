﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Web.Http;
using ProjectManagerAPI.Controllers;
using ProjectManagerEntitiesLib;

namespace TestingWebApi
{
    [TestFixture]
    public class TestWebApi
    {
        [Test]
        public void TestGetAllWebApi()
        {
            ProjectManagerController obj = new ProjectManagerController();
            List<Tasks> taskDetails = obj.GetAll();
            Assert.Greater(taskDetails.Count(), 0);
        }

        [Test]
        public void TestGetByTaskIdWebApi()
        {
            ProjectManagerController obj = new ProjectManagerController();
            Tasks item = obj.GetById(10);
            Assert.AreEqual(10, item.TaskID);
        }

        [Test]
        public void TestAddTaskWebApi()
        {
            ProjectManagerController obj = new ProjectManagerController();
            Tasks item = new Tasks();
            item.Task = "TaskName5";
            item.ParentID = 16;
            item.Priority = 9;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            IHttpActionResult ok = obj.PostTask(item);
            Tasks testItem = obj.GetByTaskName("TaskName5");
            Assert.AreEqual("TaskName5", testItem.Task);
        }

        [Test]
        public void TestDeletetaskWebApi()
        {
            ProjectManagerController obj = new ProjectManagerController();
            List<Tasks> task = obj.DeleteTask(10);
            Assert.AreEqual(1, task.Count());
        }

        [Test]
        public void TestUpdateTaskWebApi()
        {
            ProjectManagerController obj = new ProjectManagerController();
            Tasks item = new Tasks();
            item.TaskID = 3012;
            item.Task = "TaskName";
            item.Priority = 11;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.PutTask(item);
            Tasks ItemUpdate = obj.GetByTaskName("TaskName");
            Assert.AreEqual(11, ItemUpdate.Priority);
        }

        [Test]
        public void TestEndTaskWebApi()
        {
            ProjectManagerController obj = new ProjectManagerController();
            Tasks item = new Tasks();
            item.TaskID = 8;
            item.Task = "test3";
            item.Priority = 2;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.UpdateEngDate(item);
            Tasks ItemUpdate = obj.GetByTaskName("test3");
            Assert.AreEqual(1, ItemUpdate.Flag);
        }
    }
}
