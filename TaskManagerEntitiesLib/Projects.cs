﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;


namespace ProjectManagerEntitiesLib
{
    public class Projects
    {
        [Key]
        public int ProjectID { get; set; }
    
        public string Project { get; set; }
        public int Priority { get; set; }

        public int ManagerID { get; set; }
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int Flag { get; set; }


    }
}
