﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;


namespace ProjectManagerEntitiesLib
{
    public class Users
    {
        [Key]
        public int UserID { get; set; }
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int EmployeeID { get; set; }
        //public int ProjectID { get; set; }
        //public int TaskID { get; set; }
        //public int Flag { get; set; }


    }
}
