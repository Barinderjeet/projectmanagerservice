﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;


namespace ProjectManagerEntitiesLib
{
    public class ParentTasks
    {
        [Key]
        public int ParentID { get; set; }
        public string ParentTask { get; set; }
        

    }
}
