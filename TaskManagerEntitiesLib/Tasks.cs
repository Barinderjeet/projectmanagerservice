﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;


namespace ProjectManagerEntitiesLib
{
    public class Tasks
    {
        [Key]
        public int TaskID { get; set; }

        public int ParentID { get; set; }
        public int ProjectID { get; set; }
        public string Task { get; set; }
        

        public int Priority { get; set; }

        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Status { get; set; }
        public int Flag { get; set; }

        public int TaskOwner { get; set; }

    }
}
